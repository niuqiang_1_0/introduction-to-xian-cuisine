import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path:'/',
    redirect:'/login'
  },
  {
    path: '/login',
    component: () => import('../views/Login.vue'),
  },
    {
      path: '/find',
      component: () => import('../views/Find.vue'),
    },
  {
    path: '/community',
    component: () => import('../views/Community.vue'),
  }, 
  {
    path: '/plan',
    component: () => import('../views/Plan.vue'),
  }, 
  {
    path: '/me',
    component: () => import('../views/Me.vue'),
  }, 

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
